import { APIGatewayProxyResult } from 'aws-lambda';
//
import '~globally';
//
//
//
let count: number = 0;

export async function handler(): Promise<APIGatewayProxyResult> {
    count += 1;

    return {
        statusCode: 200,
        body: count.toString(),
    };
}
