import { APIGatewayProxyEvent, Context, APIGatewayProxyResult } from 'aws-lambda';
//
import '~globally';
//
//
//
export async function handler(event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> {
    const result: unknown = {
        environment,
        event,
        context,
    };

    return {
        statusCode: 200,
        body: JSON.stringify(result, null, 4),
    };
}
