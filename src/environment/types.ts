//
//
//
export interface Constants {
    readonly aws: {
        readonly region: 'ap-northeast-2';
    };
}

export interface Variables {
    readonly name: string;
}

export type Environment = Constants & Variables;
