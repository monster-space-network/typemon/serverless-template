import { merge } from '@typemon/merge';
//
import { Environment } from './types';
import { constants } from './constants';
import { variables } from './variables';
//
//
//
declare global {
    const environment: Environment;
}

Object.defineProperty(global, 'environment', {
    value: merge(constants, variables),
});
