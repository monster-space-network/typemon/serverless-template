import { Check } from '@typemon/check';
import { Configuration, BannerPlugin, NormalModuleReplacementPlugin } from 'webpack';
import Path from 'path';
import TSConfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import nodeExternals from 'webpack-node-externals';
import ServerlessWebpack from 'serverless-webpack';
//
//
//
const environment: string = ServerlessWebpack.lib.serverless.service.provider.stage;
const config: Configuration = {
    target: 'node',
    mode: 'development',
    devtool: 'inline-source-map',
    entry: ServerlessWebpack.lib.entries,
    module: {
        rules: [{
            test: /\.ts$/,
            loader: 'ts-loader',
            options: {
                configFile: 'tsconfig.compile.json',
            },
        }],
    },
    resolve: {
        plugins: [
            new TSConfigPathsPlugin(),
        ],
        extensions: [
            '.ts',
        ],
    },
    externals: [
        nodeExternals(),
    ],
};

config.plugins = [];
config.plugins.push(
    new BannerPlugin({
        banner: 'require(\'source-map-support\').install();',
        raw: true,
        entryOnly: false,
    }),
);

if (Check.isFalse(ServerlessWebpack.lib.webpack.isLocal)) {
    config.mode = 'production';
    config.devtool = 'source-map';
    config.optimization = {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    sourceMap: true,
                },
            }),
        ],
    };
}

if (Check.notEqual(environment, 'development')) {
    config.plugins.push(
        new NormalModuleReplacementPlugin(/variables\.ts/, Path.resolve('src', 'environment', `variables.${environment}.ts`)),
    );
}

export = config;
