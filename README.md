# Serverless Template
Ready-to-use TypeScript serverless application template.
It is very simple but has a strong structure.
Since linter, webpack, and serverless plugins are pre-configured, you can start without much knowledge.
Get started right now.



## Preconfigured Serverless Plugins
Pay attention to the order when adding or deleting plugins. For more information on plugins, see the plugins repository.
- [serverless-functions-base-path](https://github.com/kevinrambaud/serverless-functions-base-path)
- [serverless-webpack](https://github.com/serverless-heaven/serverless-webpack)
- [serverless-offline](https://github.com/dherault/serverless-offline)
- [serverless-plugin-split-stacks](https://github.com/dougmoscrop/serverless-plugin-split-stacks)
- [serverless-layers](https://github.com/agutoli/serverless-layers)



## Prerequisites
### AWS account and credentials
If you are an IAM user, you must have the necessary permissions for deployment.

https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-shared.html

### Node.js 12
https://nodejs.org/en/download/



## Getting Started
### 1. Clone this repository and change the remote to your own repository.
```
$ git clone https://gitlab.com/monster-space-network/typemon/serverless-template.git example
$ cd ./example
$ git remote set-url origin https://gitlab.com/example/example.git
```


### 2. Install dependencies.
Be careful not to install from a private registry.
It is recommended to update as dependencies may be out of date.
```
$ npm install
```


### 3. Configure the service name, region and time zone.
```yaml
service: example

provider:
    region: ap-northeast-2
    environment:
        TZ: Asia/Seoul
```


### 4. Create and configure lambda execution role.
https://docs.aws.amazon.com/lambda/latest/dg/lambda-intro-execution-role.html#permissions-executionrole-console
```yaml
provider:
    role: arn:aws:iam::123456789:role/example
```


### 5. Create and configure deployment bucket.
https://docs.aws.amazon.com/AmazonS3/latest/user-guide/create-bucket.html

You must create a bucket in the same region where you plan to deploy the serverless application.
```yaml
provider:
    deploymentBucket:
        name: example

custom:
    serverless-layers:
        layersDeploymentBucket: example
```


### 6. Test it locally.
See the [link](https://github.com/dherault/serverless-offline) for more information on offline features.
```
$ npm start
```


### 7. Deploy.
Use the serverless framework's deployment commands.

https://www.serverless.com/framework/docs/providers/aws/cli-reference/deploy/

https://www.serverless.com/framework/docs/providers/aws/cli-reference/deploy-function/
```
$ npx serverless deploy
```


### 8. Cleanup.
Use the serverless framework's remove command.

https://www.serverless.com/framework/docs/providers/aws/cli-reference/remove/

You must manually delete the Lambda execution role or S3 bucket you created.
```
$ npx serverless remove
```



## Environment Variables
You can deploy by stage, and environment variables are set according to the stage specified.
This feature was implemented through a webpack plugin.
If necessary, you can add stages or change existing configurations.
It is good practice to separate the same values ​​that are common across all stages into constants.
This is not a lambda environment variable. Refer to this [link](https://serverless-stack.com/chapters/serverless-environment-variables.html) for lambda environment variables configured in the serverless configuration file.
